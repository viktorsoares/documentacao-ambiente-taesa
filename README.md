# Instalação no Ambiente TAESA

## Instalações

Instale os softwares abaixos:

- [Git](https://git-scm.com/downloads)
- [NodeJS](https://nodejs.org/en/download/)
- [Dotnet](https://dotnet.microsoft.com/download)
- Após instalar o dotnet, digite no terminal ```dotnet tool install --global dotnet-ef```
- Após instalar o node, digite no terminal ```npm install -g @angular/cli```

Após instalar, teste se as versão instaladas estão corretas:

- ```node --version```
- ```dotnet --version```
- ```git --version```

## Clonar Seu Projeto

### Chave de Acesso

Para clonar o seu projeto é necessário ter a permissão SSH do bitbucket e para isso crie a chave de acesso do git.

- Gerar a chave ```ssh-keygen```
- Visualizar a chave ```cat ~/.ssh/id_rsa.pub```

Obs: Após essa etapa crie um arquivo .txt e cole a chave de acesso lá. 
E então, mande para o diego o arquivo e peça para ele dar permissão de acesso para o seu projeto no bitbucket. rs

### Permissão de Usuário

Alguns problemas poderão aparecer ao tentar clonar seu projeto, e um deles é a falta de permissão de criar pastas e arquivos.
Para solucionar isso siga as etapas abaixo:

- C:\inetpub\projects\ [nome do projeto] (se esse caminho não existir, crie);
- Clique com o botão direito e em seguida em "Properties";
- Clique na seção "Security" e no botão "Edit";
- No "Group or usernames", clique em "Users"; e
- Na seção "Permissions for CREATOR OWNER", selecione o "Full Control" e em seguida no "Ok".

[![Demo CountPages alpha](./assets/permissao-user.gif)](./assets/permissao-user.gif)


### Clone Backend

Após todas as permissões você já deve poder clonar seu projeto.

- ```git clone [url SSH] [nome do projeto]```;
- ```cd [nome do projeto]```;
- modifique o appsettings.json, alterando a string de conexão para a nova;
- ```dotnet build```;


- ```dotnet ef database update```;

### Clone Frontend

- ```git clone [url SSH] [nome do projeto]```;
- ```cd [nome do projeto]```;
- ```npm install```;

## Build do Projeto

- ```ng build --output-path ..\..\..\wwwroot\[nome do projeto]\[nome do projeto]\```
- ```dotnet publish -c Release -o ..\..\..\wwwroot\[nome do projeto]\[nome do projeto]\```;

## Configuração do HOST

[![Demo CountPages alpha](./assets/config-host.gif)](./assets/config-host.gif)
